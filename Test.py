from selenium import webdriver
driver = webdriver.Chrome(executable_path="chromedriver.exe")
driver.get("https://www.flipkart.com")
status = driver.find_element_by_xpath("//button[@class='_2KpZ6l _2doB4z']")
status.click()

try:
    driver.find_element_by_xpath("//*[contains(text(), 'Flipkart')]")
    print("Flipkart Launched success")
except:
    assert False
finally:
    driver.close()
